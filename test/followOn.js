const request = require("supertest");
const app = require("../app");
const assert = require("assert");
const url = require("url");

describe("GET /xxx/ (followOn)", () => {
  describe("GET /xxx/addition", () => {
    it("responds with correct follow on addition", () =>
      request(app)
        .get("/addition/1/1")
        .expect(200)
        .then(response => {
          let followOn = url.parse(response.body.followOn);
          return request(app)
            .get(`${followOn.path}/addition/3`)
            .expect(200)
            .then(response => {
              assert.equal(response.body.answer, 5);
            });
        }));
    it("responds with correct follow on addition from subtraction", () =>
      request(app)
        .get("/subtraction/10/1")
        .expect(200)
        .then(response => {
          let followOn = url.parse(response.body.followOn);
          return request(app)
            .get(`${followOn.path}/addition/3`)
            .expect(200)
            .then(response => {
              assert.equal(response.body.answer, 12);
            });
        }));
  });
  describe("GET /xxx/subtraction", () => {
    it("responds with correct follow on subtraction", () =>
      request(app)
        .get("/subtraction/1/1")
        .expect(200)
        .then(response => {
          let followOn = url.parse(response.body.followOn);
          return request(app)
            .get(`${followOn.path}/subtraction/3`)
            .expect(200)
            .then(response => {
              assert.equal(response.body.answer, -3);
            });
        }));
    it("responds with correct follow on (from follow on) subtraction", () =>
      request(app)
        .get("/subtraction/1/1")
        .expect(200)
        .then(response => {
          let followOn = url.parse(response.body.followOn);
          return request(app)
            .get(`${followOn.path}/subtraction/3`)
            .expect(200)
            .then(response => {
              let followOn = url.parse(response.body.followOn);
              return request(app)
                .get(`${followOn.path}/subtraction/3`)
                .expect(200)
                .then(response => {
                  assert.equal(response.body.answer, -6);
                });
            });
        }));
  });
});
