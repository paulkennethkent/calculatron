# Calculatron

The calculatron is a web API that can do basic addition and subtraction using a URL schema. It can also also return `followOn` URLs which allow further calculations in future.

## Your Task

There are tests, that are failing, that's because the service isn't filled out... so fill it in :)

## Basic Addition

The API should accept the following GET request `http://host:port/addition/13/24` and return the following body:

```javascript
{
  "answer": 37,
  "followOn": "http://host:port/xxx"
}
```

Where the `answer` is the product of adding the two numbers in the URL. See *FollowOn* for information.

## Basic Subtraction

The API should accept the following GET request `http://host:port/subtraction/43/11` and return the following body:

```javascript
{
  "answer": 32,
  "followOn": "http://host:port/xxx"
}
```

Where the `answer` is the product of subtracting the two numbers in the URL. See *FollowOn* for information.

## FollowOn

For every request made it should return a `followOn` URL (this can be designed to your own specification), which can be used to do more calculations on top of a previous answer. For example in the example `http://host:port/subtraction/43/11` the body contained the `answer` as `32` and a `followOn` url for example `http://host:port/xxx`, from this the client can then perform another request as follows `http://host:port/xxx/addition/99` which would return:

```javascript
{
  "answer": 131,
  "followOn": "http://host:port/xxx"
}
```

With another `followOn` URL, these can be chained forever.

## Running

Everything runs using standard node and `expressjs`, us the following to get your API running locally

```bash
yarn install
yarn start
```

And to run tests

```bash
yarn test
```

## Storage

If you need it 😀, please do not waste time on any kind of permenant storage, ephermeral is fine. 